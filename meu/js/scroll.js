//jQuery to collapse the navbar on scroll
//$(window).scroll(function() {
//    if ($(".menu-superior").offset().top > 80) {    	    	
//        $(".menu-superior").addClass("fixo-top");
//    } else {
//    	console.log("2");
//        $(".menu-superior").removeClass("fixo-top");
//    }
//});

//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('a.scroll-automatico').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1000, 'easeInOutExpo');
        event.preventDefault();
    });
});
